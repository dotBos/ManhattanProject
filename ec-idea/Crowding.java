
import org.vu.contest.ContestEvaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.lang.Math;
import java.util.Arrays;


public class Crowding {
    // IM STUPID MAKE PUBLIC
    public int pop_size;
    public int fam_size;
    public double prob_mut;
    public double prob_cross;
    public int gens;
    public boolean R;
    public boolean general;

    int gen_counter;

    static List<Individual> old_pop;
    static List<Individual> new_pop;
    static List<Integer> index_pool;

    Random rnd_;
    Crossover cross;
    Mutation mutans;
    static ContestEvaluation evaluation_;


    public Crowding(int pop_size,int fam_size,double prob_mut,double prob_cross,int gens, boolean general, ContestEvaluation eval)   {
        this.pop_size = pop_size;
        this.fam_size = fam_size;
        this.prob_mut = prob_mut;
        this.prob_cross = prob_cross;
        this.gens = gens;
        this.R = R;
        this.general = general;
        this.evaluation_ = eval;
        rnd_ = new Random();
        cross = new Crossover();
        // mutation_value 2 percent, for now...
        mutans = new Mutation(prob_mut,1);
        gen_counter = 0;
        index_pool = new ArrayList<Integer>();
        new_pop = new ArrayList<Individual>();
        old_pop = new ArrayList<Individual>();
        initializePopulation();
        //initializePool();



        //CrowdingGA()
    }
    //TODO: REMEMBER THAT S IS AN EVEN NUMBER OF PARENTS OR FAMILY SIZE
    public List<Individual> CrowdingGA()  {

        while (gen_counter < gens)  {
            //printGens(old_pop);

            if(general) {
                new_pop = CrowdingStep();
            }
            else    {
                new_pop = SimpleStep();
            }

            double fitness_new_population = evaluatePopulation(old_pop);
            /*
            if (gen_counter % 10 == 0)	{
                System.out.println("Total fitness of the population: " + fitness_new_population + " Iteration: " + gen_counter);
            }
            */
            old_pop = new ArrayList<Individual>(new_pop);
            gen_counter++;
        }

    return new_pop;
    }

    public void printGens(List<Individual> pop) {
        for (int i = 0; i < 4; i++) {
            System.out.println(Arrays.toString(pop.get(i).getGenotype()));
            //System.out.println("fitness: " + pop.get(i).getFitness());
        }
        System.out.println("[========] END [=========]");
    }

    public List<Individual> CrowdingStep()  {
        new_pop = new ArrayList<Individual>();
        int k = 1;
        //System.out.println("old_pop sizeeeee: " + old_pop.size());
        for (int i = 0; i < old_pop.size(); i++)    {
            index_pool.add(i);
        }

        // For 2 parents and 2 children
        double[][] distance = new double[2][2];
        while (index_pool.size() > 1)   {
            List<Individual> parents = new ArrayList<Individual>();
            List<Individual> children = new ArrayList<Individual>();
            for (int i = 0; i < fam_size; i++)  {
                int random = rnd_.nextInt(index_pool.size());
                int j = index_pool.get(random);
                Individual parent = old_pop.get(j);
                parents.add(parent);
                index_pool.remove(random);
            }
            for (int i = 0; i < fam_size; i++)  {
                double rnd_double = rnd_.nextDouble();
                if (prob_cross >= rnd_double)   {
                    children = cross.uniformCrossover(parents.get(0),parents.get(1));
                    computeSetFitness(children.get(0));
                    computeSetFitness(children.get(1));

                }
                else {

                    Individual p0 = parents.get(0);
                    Individual p1 = parents.get(1);
                    children.add(p0);
                    children.add(p1);
                }
                mutans.mutateGenes(children.get(0));
                mutans.mutateGenes(children.get(1));
            }
            for (int i = 0; i < fam_size; i++)  {
                for (int j = 0; j < fam_size; j++)  {
                    distance[i][j] = Distance(parents.get(i),children.get(j));
                }
            }

            List<Individual[]> m_star = Match(parents, children, distance);
            for (int i = 0; i < fam_size; i++)  {
                Individual winner = new Individual();
                Individual c = m_star.get(i)[1];
                Individual p = m_star.get(i)[0];
                if (ReplacementRule(p.getFitness(),c.getFitness())) {
                    winner = c;
                    //System.out.println("yokojgjhf");
                }
                else    {
                    //System.out.println("instead");
                    winner = p;
                }
                new_pop.add(winner);
                k++;
            }

        }
        return new_pop;
    }

    public List<Individual> SimpleStep()    {
        int i = 0;
        while (i < old_pop.size()) {
            System.out.println(old_pop.size());
            Individual parent = old_pop.get(i);
            Individual child = new Individual();
            double[] child_gen = parent.copyGenotype();
            child.setGenotype(child_gen);
            child.setFitness(parent.getFitness());
            mutans.mutateIndividual(child);
            if (ReplacementRule(parent.getFitness(), (double) evaluation_.evaluate(child.getGenotype()))) {
                new_pop.add(child);
            }
            else    {
                new_pop.add(parent);
            }
            i++;
        }

        return new_pop;
    }

    // TODO: NEXT TIME, WE WERE DOING DISTANCE METRIC!!!
    public double Distance(Individual parent, Individual child) {
        double[] parent_gen = parent.getGenotype();
        double[] child_gen = child.getGenotype();
        double squared_differences = 0;

        for (int i = 0; i < 10; i++)    {
            squared_differences += (parent_gen[i] - child_gen[i])*(parent_gen[i] - child_gen[i]);

        }
        return Math.sqrt(squared_differences);
    }

    public boolean ReplacementRule(double fitnessP, double fitnessC) {
        double prob = 0;
        if (fitnessP == 0 && fitnessC == 0) {
            prob = 0.5;
            return flipCoin(prob);
        }

        prob = fitnessC / (fitnessC + fitnessP);

        //System.out.println("prob: "+prob +" fitnessp: "+fitnessP+" fitnessC: " + fitnessC);
        return flipCoin(prob);
    }

    public boolean DReplacementRule(double fitnessP, double fitnessC) {
        double prob = 0;
        System.out.println(fitnessC);
        if (fitnessC * 0.75  > fitnessP)    {prob = 1;}
        else if (fitnessC > fitnessP)  {prob = 0.75;}
        else {prob = 0.25;}
        //System.out.println("probability: " + prob);
        return flipCoin(prob);
    }


    public boolean flipCoin(double p) {
        double rnd_num = rnd_.nextDouble();
        if (p > rnd_num)    {
            return true;
        }
        return false;
    }

    // TODO: GENERALIZE FOR N PARENTS?
    public List<Individual[]> Match(List<Individual> parents,List<Individual> children,double[][] distances)    {
        Individual p0 = parents.get(0); Individual p1 = parents.get(1);
        Individual c0 = children.get(0); Individual c1 = children.get(1);

        Individual[] pc1 = new Individual[]{p0,c0};Individual[] pc2 = new Individual[]{p1,c1};
        Individual[] pc3 = new Individual[]{p0,c1};Individual[] pc4 = new Individual[]{p1,c0};

        List<Individual[]> m1 = new ArrayList<Individual[]>();
        List<Individual[]> m2 = new ArrayList<Individual[]>();
        m1.add(pc1); m1.add(pc2); m2.add(pc3); m2.add(pc4);

        double d1 = distances[0][0] + distances[1][1];
        double d2 = distances[0][1] + distances[1][0];
        if (d1 < d2)    {return m1;}
        return m2;
    }

    public void initializePool()    {
        for (int i = 0; i < this.pop_size; i++)  {
            index_pool.add(i);
        }
    }

    // TODO: CHECK IF GOOD IDEA TO COPY PASTE CODE INSTEAD OF GENERAL FUNC
    public void initializePopulation()    {
        for (int i = 0; i < this.pop_size; i++) {
            Individual p = new Individual();
            p.generateIndividual();
            old_pop.add(p);
        }
    }

    public static double evaluatePopulation(List<Individual> pop)	{
        double total_fitness = 0;

        for (Individual individual:pop) {
            double fitness = computeFitness(individual);
            individual.setFitness(fitness);
            total_fitness += fitness;
        }
        return total_fitness;
    }
    /***********************************************************************
     * FITNESS
     ************************************************************************/
    public static double computeFitness(Individual individual){
        double[]  genotype = individual.getGenotype();
        double fitness = (double) evaluation_.evaluate(genotype);
        return fitness;
    }

    public void computeSetFitness(Individual individual)  {
        double fit = computeFitness(individual);
        individual.setFitness(fit);
    }

}
