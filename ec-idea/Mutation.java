import java.util.Random;
import java.util.List;

public class Mutation {
    private Random rnd_;
    double mutation_chance;
    double mutation_value;

    // The object takes a probability for mutation (mutation_chance)
    // and the percentage the value must be mutated
    public Mutation(double mutation_chance, double mutation_value)   {
        this.mutation_chance = mutation_chance;
        this.mutation_value = mutation_value;
        rnd_ = new Random();
    }

    // Mutation the individual given the mutation_value and chance
    // The double change mutates one of the (randomly chosen) alleles
    public Individual mutateIndividual(Individual individual) {
        double[] genotype = individual.getGenotype();
        double random_value = rnd_.nextDouble();
        int mutation_position = rnd_.nextInt(10);
        int random_choice = rnd_.nextInt(9);
        if (mutation_chance >= random_value)    {
            double change = (rnd_.nextDouble() * -mutation_value) + (rnd_.nextDouble() * mutation_value);
            genotype[mutation_position] = genotype[mutation_position] + change;
        }
        individual.setGenotype(genotype);
        return individual;
    }

    public Individual mutateGenes(Individual individual)    {
        double[] genotype = individual.getGenotype();
        for (int i = 0; i < 10; i++)    {
            double random_value = rnd_.nextDouble();
            if (mutation_chance >= random_value)    {
                double change = (rnd_.nextDouble() * -mutation_value) + (rnd_.nextDouble() * mutation_value);
                genotype[i] = genotype[i] + change;
            }
        }
        individual.setGenotype(genotype);
        return individual;
    }


    // Mutate the whole population
    public List<Individual> mutatePopulation(List<Individual> population)  {
        for(Individual individual : population) {
            Individual mutated_individual = mutateIndividual(individual);
        }
        return population;
    }

    // Mutate the whole population
    public List<Individual> mutatePopulationGenes(List<Individual> population)  {
        for(Individual individual : population) {
            Individual mutated_individual = mutateGenes(individual);
        }
        return population;
    }


}
