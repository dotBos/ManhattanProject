import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Selection {

    public Selection() {

    }

    public Individual fitnessProportionateSelection(List<Individual> population, double total_fitness) {
        Random rnd = new Random();
        double random_choice = rnd.nextDouble() * total_fitness;
        Collections.sort(population, Collections.reverseOrder());

        Individual individual;
        int index = 0;

        while(true) {
            individual = population.get(index);
            random_choice -= individual.getFitness();
            if (random_choice < 0) break;
            index++;
        }

        return individual;
    }

    public Individual tournamentSelection(List<Individual> population, int tournament_size) {
        Individual best = null;
        Random rnd = new Random();
        int random_choice;

        for (int i = 0; i < tournament_size; i++) {
            random_choice = rnd.nextInt(population.size());
            Individual individual = population.get(random_choice);

            if (best == null || best.getFitness() < individual.getFitness()) best = individual;
        }
        return best;
    }
}
