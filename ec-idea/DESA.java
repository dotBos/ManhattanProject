
import org.vu.contest.ContestEvaluation;

import java.io.*;
import java.util.*;
import java.lang.Math;

public class DESA {
    static ContestEvaluation evaluation_;
    int pop_size;
    public static List<Individual> population;
    double F; // Differential Weight
    double cross_prob;
    int gens;
    public static int evals = 0;
    Random rnd_;
    private static String path;

    public DESA(double F, int gens, int pop_size, double cross_prob, ContestEvaluation evaluation_) {
        this.evaluation_ = evaluation_;
        this.pop_size = pop_size;
        this.population = new ArrayList<Individual>();
        this.F = F;
        this.cross_prob = cross_prob;
        this.gens = gens;
        rnd_ = new Random();
        initializePopulation();
        double initial_fitness = evaluatePopulation(population);
        System.out.println("Initial fitness = " + initial_fitness);
    }

    public List<Individual> DeGeneration()   {
        Properties props = evaluation_.getProperties();
        int eval_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        for (int i = 0; i < (eval_limit_/pop_size -1); i++)  {
            DifferentialEvolution();
        }
        return population;
    }

    public void DifferentialEvolution() {

        for (Individual x : population) {
            Individual a,b,c;
            ArrayList<Individual> subpopulation = new ArrayList<>(population);
            subpopulation.remove(x);

            Collections.shuffle(subpopulation);
            a = subpopulation.get(0);
            b = subpopulation.get(1);
            c = subpopulation.get(2);

            // Mutation
            int R = rnd_.nextInt(10); // random_index
            double[] new_genotype = new double[10];

            // Adapt F
            double newF = x.getF() * Math.exp(rnd_.nextGaussian()* x.getF());

            // Crossover
            for (int i = 0; i < 10; i++)    {
                double r_i = rnd_.nextDouble();
                if (r_i < cross_prob || i == R) {
                    new_genotype[i] = a.getGenotype()[i] + newF * (b.getGenotype()[i] - c.getGenotype()[i]);
                }
                else    {
                    new_genotype[i] = x.getGenotype()[i];
                }
            }
            double new_genotype_fitness = (double) evaluation_.evaluate(new_genotype);
            // Selection
            if (new_genotype_fitness > x.getFitness())  {
                x.setFitness(new_genotype_fitness);
                x.setGenotype(new_genotype);
                x.setF(newF);
            }
        }
    }


    // THESE ARE COPY PASTED FROM OUR PREVIOUS CODE
    // TODO: CHECK IF GOOD IDEA TO COPY PASTE CODE INSTEAD OF GENERAL FUNC
    public void initializePopulation()    {
        for (int i = 0; i < this.pop_size; i++) {
            Individual p = new Individual();
            p.generateIndividual();
            population.add(p);
        }
    }

    public static double evaluatePopulation(List<Individual> pop)	{
        double total_fitness = 0;

        for (Individual individual:pop) {
            double fitness = computeFitness(individual);
            individual.setFitness(fitness);
            total_fitness += fitness;
        }
        return total_fitness;
    }
    /***********************************************************************
     * FITNESS
     ************************************************************************/
    public static double computeFitness(Individual individual){
        double[]  genotype = individual.getGenotype();
        double fitness = (double) evaluation_.evaluate(genotype);
        return fitness;
    }

    public void computeSetFitness(Individual individual)  {
        double fit = computeFitness(individual);
        individual.setFitness(fit);
    }
}
