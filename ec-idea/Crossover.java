import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;


import javax.swing.*;
import java.util.Arrays;
import java.util.Random;
import java.util.Properties;
import java.util.ArrayList;
import java.util.List;

public class Crossover {
    // Properties
    // p_x := point of crossover
    int p_x;
    Random rnd_;

    public Crossover()  {
        rnd_ = new Random();

    }

    public List<Individual> uniformCrossover(Individual parent1, Individual parent2) {
        double[] parent1_gen = parent1.getGenotype(), parent2_gen = parent2.getGenotype();
        double[] child1_gen = new double[10], child2_gen = new double[10];
        int coin_toss;

        for(int i = 0; i < 10; i++) {
            coin_toss = rnd_.nextInt(2);

            switch (coin_toss) {
                case 0:
                    child1_gen[i] = parent1_gen[i];
                    child2_gen[i] = parent2_gen[i];
                    break;
                case 1:
                    child1_gen[i] = parent2_gen[i];
                    child2_gen[i] = parent1_gen[i];
                    break;
            }
        }

        List<Individual> children = new ArrayList<Individual>(2);
        Individual child1 = new Individual(), child2 = new Individual();
        child1.setGenotype(child1_gen);
        child2.setGenotype(child2_gen);
        children.add(child1);
        children.add(child2);

        return children;
    }

    /*
    This default method takes two individuals and uses the cross over point (p_x) [2,8]
    to determine where to cut te genotypes
    */
    public List<Individual> cutAndCrossfill(Individual p1, Individual p2) {
        // Get the genotypes
        double[] p1_gen = p1.getGenotype();
        double[] p2_gen = p2.getGenotype();

        // initialize the new genotypes (both children)
        Individual child1 = new Individual();
        Individual child2 = new Individual();
        double[] child1_gen, child2_gen;
        child1_gen = new double[10]; child2_gen = new double[10];
        int p_x = 1 + rnd_.nextInt(8);
        // Fill in the first part up to p_x
        for(int i = 0; i < p_x; i++)    {
            child1_gen[i] = p1_gen[i];
            child2_gen[i] = p2_gen[i];
        }

        // Fill in the second part up to the end  of the array from p_x
        for(int i = 0; i < 10 - p_x; i++)   {
            child1_gen[p_x + i] = p2_gen[p_x + i];
            child2_gen[p_x + i] = p1_gen[p_x + i];
        }
        // Now update the genotype of the indivudal
        child1.setGenotype(child1_gen);
        child2.setGenotype(child2_gen);
        return Arrays.asList(child1,child2);
    }
}
