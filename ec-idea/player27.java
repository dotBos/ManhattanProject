//import com.sun.org.apache.bcel.internal.generic.POP;
import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.directory.InitialDirContext;
import javax.swing.*;
import java.util.*;
//import BentCigarFunction;

public class player27 implements ContestSubmission {
	public static Random rnd_;
	static ContestEvaluation evaluation_;
	static int evaluations_limit_;
	static List<Individual> population;

	// Crowding parameters

	static int FAMILY_SIZE;
	static double PROB_MUT;
	static double PROB_CROSS;
	static int GENERATIONS;
	static boolean REPLACEMENT;
	static boolean GENERAL;

	public static double F;
	public static int pop_size;
	public static double cross_prob;
	public static int gens;

    private static String path;
	// Population parameters
	public static int POPULATION_SIZE;

	public static List<double[]> e_f_x;

	public player27() {

		rnd_ = new Random();
	}

	public static void main(String[] args) throws IOException {

	}

	public void setSeed(long seed) {
		// Set seed of algortihms random process
		rnd_.setSeed(seed);
	}

	public void setEvaluation(ContestEvaluation evaluation) {
		// Set evaluation problem used in the run
		evaluation_ = evaluation;

		// Get evaluation properties
		Properties props = evaluation.getProperties();
		// Get evaluation limit
		evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
		// Property keys depend on specific evaluation
		// E.g. double param = Double.parseDouble(props.getProperty("property_name"));
		boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
		boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
		boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

		// Do sth with property values, e.g. specify relevant settings of your algorithm
		if (!isMultimodal && !hasStructure && !isSeparable) {
			System.out.println("BentCigarFunction");
			this.F = 0.45; this.gens = ((int) 10000/30) - 2; this.pop_size = 30; this.cross_prob = 0.90;
		}
		else if (isMultimodal && !hasStructure && !isSeparable) {
			System.out.println("KatsuuraFunction");
			this.F = 0.45; this.gens = ((int) 1000000/200) - 2; this.pop_size = 200; this.cross_prob = 0.5;
		}
		else if (isMultimodal && hasStructure && !isSeparable) {
			System.out.println("SchaffersFunction");
			this.F = 0.44; this.gens = (100000/30) - 2; this.pop_size = 30; this.cross_prob = 0.5;

		}else {
			POPULATION_SIZE = 100;
		}
	}

	/***********************************************************************
	 * POPULATION
	 ************************************************************************/
	public static void initializePopulation()    {
		population = new ArrayList<Individual>();
		for (int i = 0; i < POPULATION_SIZE; i++) {
			Individual p = new Individual();
			p.generateIndividual();
			population.add(p);

		}
	}
	public static double evaluatePopulation(List<Individual> pop)	{
		double total_fitness = 0;

		for (Individual individual:pop) {
			double fitness = computeFitness(individual);
			individual.setFitness(fitness);
			total_fitness += fitness;
		}
		return total_fitness;
	}
	/***********************************************************************
	 * FITNESS
	 ************************************************************************/
	public static double computeFitness(Individual individual){
		double[]  genotype = individual.getGenotype();
		double fitness = (double) evaluation_.evaluate(genotype);
		return fitness;
	}

	/***********************************************************************
	 * ALGORITHM
	 ************************************************************************/
	public void run() {
		// Run your algorithm here
		DE Devol = new DE(F, gens, pop_size, cross_prob, evaluation_);
		Devol.DeGeneration();

	}
    private static void printResults(List<double[]> e_f_x) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (double[] res : e_f_x)  {
            sb.append(res[0]+";"+res[1] +";" + res[2]+";" + res[3]);
            sb.append('\n');
        }

        FileWriter write = new FileWriter(path, true);
        PrintWriter print = new PrintWriter(write);
        print.println(sb.toString());
        print.close();
    }

    private static Individual getBestIndividual(List<Individual> population)    {
	    Individual elite = population.get(0);
	    for (Individual ind : population)   {
	        if (ind.getFitness() > elite.getFitness())  {
	            elite = ind;
            }
        }
        return elite;
    }


}

