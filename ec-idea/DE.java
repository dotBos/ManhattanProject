
import org.vu.contest.ContestEvaluation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.lang.Math;
import java.util.Arrays;

public class DE {
    private static ContestEvaluation evaluation_;
    private int pop_size;
    public static List<Individual> population;
    private double F; // Differential Weight
    private double cross_prob;
    private int gens;
    private int current_gen;
    private Random rnd_;

    public DE(double F, int gens, int pop_size, double cross_prob, ContestEvaluation evaluation_) {
        this.evaluation_ = evaluation_;
        this.pop_size = pop_size;
        this.population = new ArrayList<Individual>();
        this.F = F;
        this.cross_prob = cross_prob;
        this.gens = gens;
        rnd_ = new Random();
        initializePopulation();
        double initial_fitness = evaluatePopulation(population);
    }

    public void DeGeneration()   {
        for (int i = 0; i < gens; i++)  {
            DifferentialEvolution();
        }
    }

    public void DifferentialEvolution() {

        for (Individual x : population) {
            Individual a,b,c;
            a = new Individual(); b = new Individual(); c = new Individual();
            int count = 0;
            // 5 because true was spacing
            while (count < 5)   {
                count++;

                //System.out. println(three_list.size());
                int rnd_index1 = rnd_.nextInt(pop_size);
                int rnd_index2 = rnd_.nextInt(pop_size);
                int rnd_index3 = rnd_.nextInt(pop_size);

                a = population.get(rnd_index1);
                b = population.get(rnd_index2);
                c = population.get(rnd_index3);
                if (!a.equals(b) && !a.equals(x) && !b.equals(x) && !b.equals(c) && !c.equals(x) && !c.equals(a))   {
                    break;
                }
            }
            // Mutation
            int R = rnd_.nextInt(10); // random_index
            double[] new_genotype = new double[10];

            // Crossover
            for (int i = 0; i < 10; i++)    {
                double r_i = rnd_.nextDouble();
                if (r_i < cross_prob || i == R) {
                    new_genotype[i] = a.getGenotype()[i] + F * (b.getGenotype()[i] - c.getGenotype()[i]);
                }
                else    {
                    new_genotype[i] = x.getGenotype()[i];
                }
            }
            double new_genotype_fitness = (double) evaluation_.evaluate(new_genotype);
            // Selection
            if (new_genotype_fitness > x.getFitness())  {
                x.setFitness(new_genotype_fitness);
                x.setGenotype(new_genotype);
            }
        }
    }



    // THESE ARE COPY PASTED FROM OUR PREVIOUS CODE
    // TODO: CHECK IF GOOD IDEA TO COPY PASTE CODE INSTEAD OF GENERAL FUNC
    public void initializePopulation()    {
        for (int i = 0; i < this.pop_size; i++) {
            Individual p = new Individual();
            p.generateIndividual();
            population.add(p);
        }
    }

    public static double evaluatePopulation(List<Individual> pop)	{
        double total_fitness = 0;

        for (Individual individual:pop) {
            double fitness = computeFitness(individual);
            individual.setFitness(fitness);
            total_fitness += fitness;
        }
        return total_fitness;
    }
    /***********************************************************************
     * FITNESS
     ************************************************************************/
    public static double computeFitness(Individual individual){
        double[]  genotype = individual.getGenotype();
        double fitness = (double) evaluation_.evaluate(genotype);
        return fitness;
    }

    public void computeSetFitness(Individual individual)  {
        double fit = computeFitness(individual);
        individual.setFitness(fit);
    }

    private double crossProb() {
        return cross_prob - ((current_gen * (1/((double) gens + 1))) * cross_prob);
    }

    private double F() {
        return F - ((current_gen * (1/((double) gens + 1))) * F);
    }


}
