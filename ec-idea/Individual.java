
import java.util.Random;
public class Individual implements Comparable<Individual>{
    private double[] genotype;
    private double fitness;
    private double F;

    public Individual() {
        genotype = new double[10];
    }

    public void generateIndividual()  {
        Random rnd_ = new Random();
        for(int i=0; i<10;i++)  {
            this.genotype[i] = -5 + (rnd_.nextDouble() * 10);
        }
        F = rnd_.nextDouble();
        while (F == 0){
            F = rnd_.nextDouble();
        }
    }

    /***********************************************************************
     * GENOTYPE METHODS
     ************************************************************************/
    public double[] getGenotype() { return this.genotype; }

    public void setGenotype(double[] new_genotype)   {this.genotype = new_genotype;}

    /***********************************************************************
     * FITNESS METHODS
     ************************************************************************/
    public double getFitness()  {return this.fitness;}

    public void setFitness(double fitness)  {this.fitness = fitness;}

    /***********************************************************************
     * CLASS METHODS
     ************************************************************************/
    public int compareTo(Individual individual){
        double fitness1 = this.fitness;
        double fitness2 = individual.fitness;

        return fitness1 == fitness2 ? 0 : (fitness1 > fitness2 ? 1 : -1);
    }

    public double[] copyGenotype()    {
        double[] copy_gen = new double[10];
        for (int i = 0; i < 10; i++)    {
            copy_gen[i] = this.genotype[i];
        }
        return copy_gen;
    }

    public boolean equals(Individual ind)   {
        for (int i = 0; i < 10; i++)    {
            if (this.genotype[i] != ind.getGenotype()[i]) {
                return false;
            }
        }
        return true;
    }

    public double getF(){
        return F;
    }

    public void setF(double f){
        F = f;
    }

}

